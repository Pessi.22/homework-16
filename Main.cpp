#include <iostream>

int main()
{
    const int N = 5;
    int array[N][N] = {};

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    int sum = 0; 
    time_t t;
    time(&t);
    int day = localtime(&t)->tm_mday; 

    for(int x = 0; x < N; x++)
    { 
        sum += array [day % N] [x];
    } 
    std::cout << sum << std::endl;

 return 0;
}